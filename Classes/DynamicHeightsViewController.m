//
//  DynamicHeightsViewController.m
//  DynamicHeights
//
//  Created by Matt Long on 9/22/09.
//  Copyright Skye Road Systems, Inc. 2009. All rights reserved.
//

#import "DynamicHeightsViewController.h"

#define CELL_FONT [UIFont fontWithName:@"HelveticaNeue" size:15]

static const CGFloat CELL_IMAGE_VIEW_WIDTH = 40.0f;
static const CGFloat CELL_ACCESSORY_WIDTH = 30.0f;
static const CGFloat CELL_HEIGHT_INSET = 10.0f;

@implementation DynamicHeightsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    items = [[NSMutableArray alloc] init];
    
    [items addObject:@"Happiness is having a large, loving, car aoe ao uoeu ao uao uao uao uaoe uao uao uaoeu aoue aoue aou aoue aou ao eing, close-knit family in another city."];
    [items addObject:@"When I am abroad, I always make it a rule never to criticize or attack the government of my own country. I make up for lost time when I come home."];
    [items addObject:@"After two years in Washington, I often long for the realism and sincerity of Hollywood. Fred Thompson, Speech before the Commonwealth Club of California"];
    [items addObject:@"It is a profitable thing, if one is wise, to seem foolish. Aeschylus (525 BC - 456 BC)"];
    [items addObject:@"Bill Gates is a very rich man today... and do you want to know why? The answer is one word: versions. Dave Barry"];
    [items addObject:@"At the worst, a house unkept cannot be so distressing as a life unlived. Dame Rose Macaulay (1881 - 1958)"];
    [items addObject:@"It is curious aouaoe aoe aouoe that physical courage should be so common in the world and moral courage so rare. Mark Twain (1835 - 1910)"];
    [items addObject:@"The knowledge of the world is only to be acquired in the world, and not in a closet. Lord Chesterfield (1694 - 1773), Letters to His Son, 1746, published 1774"];
    [items addObject:@"What"];
}

#pragma mark - UITableView Delegaates

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
	return [items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return [self frameForLabelAtIndexPath:indexPath].size.height + CELL_HEIGHT_INSET * 2;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = CELL_FONT;
        cell.imageView.image = [UIImage imageNamed: @"orange_circle.png"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.text = [items objectAtIndex:[indexPath row]];
    cell.textLabel.frame = [self frameForLabelAtIndexPath:indexPath];
    
    return cell;
}

- (CGRect)frameForLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *text = [items objectAtIndex:[indexPath row]];
    CGFloat labelWidth = self.view.frame.size.width - (CELL_ACCESSORY_WIDTH + CELL_IMAGE_VIEW_WIDTH);
    CGSize constraint = CGSizeMake(labelWidth, 20000.0f);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@{
                                                              NSFontAttributeName:CELL_FONT}];
    
    CGRect rect = [attributedText boundingRectWithSize:constraint
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    return CGRectMake(CELL_IMAGE_VIEW_WIDTH, CELL_HEIGHT_INSET, labelWidth, rect.size.height);
}

@end